package Chapter5;

public class Section_5_1 {

    public static void main(String[] args) {

        final int PER_LINE = 10;

        for (int i = 1; i < 100; i++) {
            int num = getNumber(i);
            if (i% PER_LINE == 0) {
                System.out.println(num);
            } else {
                System.out.print(num);
            }
        }
    }

    public static int getNumber(int n) {
        return n * (n - 1) / 2;
    }
}
