package Chapter5;

import java.util.Scanner;

public class Section_5_2 {

    static long sum = 1;

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        long num = input.nextInt();
        int ans = productDigits(num);
        System.out.println("Product of digits is " + ans);
    }

    public static int productDigits(long n) {

        while (n != 0) {
            sum *= n % 10;
            n = n / 10;
        }
        return (int) sum;
    }

}
