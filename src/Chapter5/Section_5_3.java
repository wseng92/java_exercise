package Chapter5;

import java.util.Scanner;

public class Section_5_3 {

    static int a;
    static int b = 0;
    static int palindrome;

    public static void main(String[] args) {

        Scanner num = new Scanner(System.in);
        int value = num.nextInt();
        palindrome = value;

        int reverseNum = reverse(value);
        System.out.println("Reverse number is " + reverseNum);
        boolean check = isPalindrome(reverseNum);
        if (check == true) {
            System.out.println("It is palindrome.");
        } else {
            System.out.println("It is not a palindrome.");
        }
    }

    public static int reverse(int number) {

        while (number > 0) {
            a = number % 10;
            b = b * 10 + a;
            number = number / 10;
        }
        return b;
    }

    public static boolean isPalindrome(int num) {

        if (palindrome == num) {
            return true;
        } else {
            return false;
        }
    }
}
