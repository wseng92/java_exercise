package Chapter5;

import java.util.Scanner;

public class Section_5_4 {


    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int value = input.nextInt();
        reverse(value);
    }

    public static void reverse(int number) {
        int b = 0;
        while (number > 0) {
            int a = number % 10;
            b = b * 10 + a;
            System.out.println("s "+b);
            number = number / 10;
        }
        System.out.println(b);
    }
}
