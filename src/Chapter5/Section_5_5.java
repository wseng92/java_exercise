package Chapter5;

import java.util.Scanner;

public class Section_5_5 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        double num1 = input.nextDouble();
        double num2 = input.nextDouble();
        double num3 = input.nextDouble();
        displayLargestNumber(num1, num2, num3);
    }

    public static void displayLargestNumber(double num1, double num2, double num3) {
        double largestNum = num1;

        if (num2 > largestNum) {
            largestNum = num2;

            if (num3 > largestNum) {
                largestNum = num3;
            }
        }
        System.out.println("Largest number is " + largestNum);
    }
}
